
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author João Paulo
 */
public class Pratica43 {
    public static void main(String[] args) {
        
        Elipse e1 = new Elipse(2.5, 3.5);
        Elipse e2 = new Elipse(2, 5);
        Circulo c1 = new Circulo(5);
        Circulo c2 = new Circulo(1.5);
        Retangulo r1 = new Retangulo(4,10);
        Retangulo r2 = new Retangulo(2.5,5.5);
        Quadrado q1 = new Quadrado(7);
        Quadrado q2 = new Quadrado(3.3);
        TrianguloEquilatero t1 = new TrianguloEquilatero(6);
        TrianguloEquilatero t2 = new TrianguloEquilatero(6.6);
        

        System.out.println(e1.getNome()+" 1\t\t: P = "+e1.getPerimetro()+" \t| A = "+e1.getArea());
        System.out.println(e2.getNome()+" 2\t\t: P = "+e2.getPerimetro()+" \t| A = "+e2.getArea()); 
        System.out.println(c1.getNome()+" 1\t\t: P = "+c1.getPerimetro()+" \t| A = "+c1.getArea());
        System.out.println(c2.getNome()+" 2\t\t: P = "+c2.getPerimetro()+" \t\t| A = "+c2.getArea());
        System.out.println(r1.getNome()+" 1\t\t: P = "+r1.getPerimetro()+" \t\t\t| A = "+r1.getArea());
        System.out.println(r2.getNome()+" 2\t\t: P = "+r2.getPerimetro()+" \t\t\t| A = "+r2.getArea()); 
        System.out.println(q1.getNome()+" 1\t\t: P = "+q1.getPerimetro()+" \t\t\t| A = "+q1.getArea());
        System.out.println(q2.getNome()+" 2\t\t: P = "+q2.getPerimetro()+" \t\t\t| A = "+q2.getArea());
        System.out.println(t1.getNome()+" 1\t: P = "+t1.getPerimetro()+" \t\t\t| A = "+t1.getArea());
        System.out.println(t2.getNome()+" 2\t: P = "+t2.getPerimetro()+" \t| A = "+t2.getArea());
    }
}
