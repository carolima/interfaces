/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author João Paulo
 */
public interface FiguraComEixos extends Figura {
    
    @Override
    public String getNome();
    @Override
    public double getPerimetro();
    @Override
    public double getArea();
    public double getEixoMenor();
    public double getEixoMaior();
}
