/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author João Paulo
 */
public class Elipse implements FiguraComEixos {
    protected double r;
    protected double s;
    
    public Elipse(double r, double s){
        this.r = r;
        this.s = s;
    }    
    
    @Override
    public double getEixoMenor() {
        if(r<s)
            return r;
        else
            return s;
    }

    @Override
    public double getEixoMaior() {
         if(r>s)
            return r;
        else
            return s;
    }
    
    @Override
    public double getArea()
    {
        return Math.PI*getEixoMenor()*getEixoMaior();
    }
    
    @Override
    public double getPerimetro(){
        return Math.PI*(3*(getEixoMenor()+getEixoMaior())-Math.sqrt((3*getEixoMenor()+getEixoMaior())*(getEixoMenor()+3*getEixoMaior())));
    }  
    
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }
}
